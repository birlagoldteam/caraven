import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { DashboardComponent }   from './dashboard/dashboard.component';
import { HomeComponent } from './home/home.component';

import { AppHeaderComponent } from './layouts/app-header/app-header.component';
import { AppFooterComponent } from './layouts/app-footer/app-footer.component';
import { AppLayoutComponent } from './layouts/app-layout/app-layout.component';
import { AppSectionComponent } from './app-section/app-section.component';

import { KssBoxOfficeAboutUsComponent } 
 from './kss-box-office-about-us/kss-box-office-about-us.component';

import { KssBoxOfficeContactUsComponent } 
 from './kss-box-office-contact-us/kss-box-office-contact-us.component';

import { KssBoxOfficeBusinessOpportunityComponent } 
  from './kss-box-office-business-opportunity/kss-box-office-business-opportunity.component';

import { KssBoxOfficeDistributionComponent } 
  from './kss-box-office-distribution/kss-box-office-distribution.component';

import { KssBoxOfficeFilmExhibitionComponent } 
  from './kss-box-office-film-exhibition/kss-box-office-film-exhibition.component';

import { KssBoxOfficeFilmyCarvanComponent } 
  from './kss-box-office-filmy-caravan/kss-box-office-filmy-caravan.component';

import { KssBoxOfficeManagementTeamComponent } 
  from './kss-box-office-management-team/kss-box-office-management-team.component';

import { KssBoxOfficeMovieDetailsComponent } 
  from './kss-box-office-movie-details/kss-box-office-movie-details.component';

import { KssBoxOfficeMoviesScheduleComponent } 
 from './kss-box-office-movies-schedule/kss-box-office-movies-schedule.component';

import { KssBoxOfficeProductionComponent } 
     from './kss-box-office-production/kss-box-office-production.component';

import { KssBoxOfficeWhatWeDoComponent } 
     from './kss-box-office-what-we-do/kss-box-office-what-we-do.component';

import { KssBoxOfficeWhoWeAreComponent } 
     from './kss-box-office-who-we-are/kss-box-office-who-we-are.component';

import { LoginComponent } 
     from './login/login.component';

import { MembershipRegistrationComponent } 
     from './membership-registration/membership-registration.component';

import { RegisterComponent } 
     from './register/register.component';


const routes: Routes = [
  // { path: 'login', loadChildren: './auth/login/login.module#LoginModule' },
  // { path: 'forgot', loadChildren: './auth/forgot/forgot.module#ForgotModule' },
  // { path: 'reset', loadChildren: './auth/reset/reset.module#ResetModule' },
  {

   path: '', 
   component: AppLayoutComponent,
   children: [
 
  { path: '', component: AppSectionComponent, pathMatch: 'full'},
  // { path: '', redirectTo: '/home', pathMatch: 'full'},
  // { path: 'home',  component: HomeComponent },
  // { path: 'dashboard',  component: DashboardComponent },

  
  { path: 'kss-box-office-about-us', component: KssBoxOfficeAboutUsComponent },
  { path: 'kss-box-office-what-we-do', component: KssBoxOfficeWhatWeDoComponent },
  { path: 'kss-box-office-who-we-are', component: KssBoxOfficeWhoWeAreComponent },
  { path: 'kss-box-office-production', component: KssBoxOfficeProductionComponent },
  { path: 'kss-box-office-distribution', component: KssBoxOfficeDistributionComponent },
  { path: 'kss-box-office-film-exhibition', component: KssBoxOfficeFilmExhibitionComponent },
  { path: 'kss-box-office-filmy-caravan', component: KssBoxOfficeFilmyCarvanComponent },
  { path: 'kss-box-office-management-team', component: KssBoxOfficeManagementTeamComponent },
  { path: 'kss-box-office-business-opportunity', component: KssBoxOfficeBusinessOpportunityComponent },
  { path: 'kss-box-office-contact-us', component: KssBoxOfficeContactUsComponent },
  { path: 'membership-registration', component: MembershipRegistrationComponent },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent }

  
    ]
  }

  // { path: 'dashboard', component: DashboardComponent },
  // { path: 'dashboard', component: DashboardComponent },
  // { path: 'dashboard', component: DashboardComponent }

  // App routes goes here here
    // { 
    //     path: '',
    //     component: AppLayoutComponent, 
    //     children: [
    //       { path: 'dashboard', component: DashboardComponent },
    //       { path: 'profile', component: ProfileComponent }
    //     ]
    // },

    //no layout routes
    // { path: 'login', component: LoginComponent},
    // { path: 'register', component: RegisterComponent },
    // // otherwise redirect to home
    // { path: '**', redirectTo: '' }


  

];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}

