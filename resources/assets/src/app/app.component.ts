import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';

@Component({
  selector: 'app-root',
  template: `
  <!-- <app-navbar></app-navbar> -->
  <!-- <app-header></app-header> -->
  <!-- <app-section></app-section>  -->

  <router-outlet></router-outlet>

  <!-- <app-section></app-section> -->
  <!-- <app-footer></app-footer>  -->
 `,
  styleUrls: ['./app.component.css'],
})
export class AppComponent {

}

