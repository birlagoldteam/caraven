import { NgModule }       from '@angular/core';
import { BrowserModule }  from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule }    from '@angular/forms';
import { HttpModule } from '@angular/http';
import { Http, Headers } from '@angular/http';
import { Directive, Component, OnInit, Input } from '@angular/core';
import 'rxjs/add/operator/toPromise';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent }         from './app.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { HomeComponent } from './home/home.component';
import { NavbarComponent } from './navbar/navbar.component';
import { HttpService } from './services/http.service';

import { AppHeaderComponent } from './layouts/app-header/app-header.component';
import { AppFooterComponent } from './layouts/app-footer/app-footer.component';
import { AppLayoutComponent } from './layouts/app-layout/app-layout.component';
import { AppSectionComponent } from './app-section/app-section.component';

import { KssBoxOfficeAboutUsComponent } 
 from './kss-box-office-about-us/kss-box-office-about-us.component';

import { KssBoxOfficeContactUsComponent } 
 from './kss-box-office-contact-us/kss-box-office-contact-us.component';

import { KssBoxOfficeBusinessOpportunityComponent } 
  from './kss-box-office-business-opportunity/kss-box-office-business-opportunity.component';

import { KssBoxOfficeDistributionComponent } 
  from './kss-box-office-distribution/kss-box-office-distribution.component';

import { KssBoxOfficeFilmExhibitionComponent } 
  from './kss-box-office-film-exhibition/kss-box-office-film-exhibition.component';

import { KssBoxOfficeFilmyCarvanComponent } 
  from './kss-box-office-filmy-caravan/kss-box-office-filmy-caravan.component';

import { KssBoxOfficeManagementTeamComponent } 
  from './kss-box-office-management-team/kss-box-office-management-team.component';

import { KssBoxOfficeMovieDetailsComponent } 
  from './kss-box-office-movie-details/kss-box-office-movie-details.component';

import { KssBoxOfficeMoviesScheduleComponent } 
 from './kss-box-office-movies-schedule/kss-box-office-movies-schedule.component';

import { KssBoxOfficeProductionComponent } 
     from './kss-box-office-production/kss-box-office-production.component';

import { KssBoxOfficeWhatWeDoComponent } 
     from './kss-box-office-what-we-do/kss-box-office-what-we-do.component';

import { KssBoxOfficeWhoWeAreComponent } 
     from './kss-box-office-who-we-are/kss-box-office-who-we-are.component';

import { LoginComponent } 
     from './login/login.component';

import { MembershipRegistrationComponent } 
     from './membership-registration/membership-registration.component';

import { RegisterComponent } 
     from './register/register.component';

// import { KssBoxOfficeAboutUsComponent } 
//      from './kss-box-office-about-us/kss-box-office-about-us.component';


@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    HttpModule
  ],
  declarations: [
    AppComponent,
    DashboardComponent,
    HomeComponent,
    NavbarComponent,
    AppHeaderComponent,
    AppFooterComponent,
    AppLayoutComponent,
    AppSectionComponent,
    KssBoxOfficeAboutUsComponent,
    KssBoxOfficeContactUsComponent,
    KssBoxOfficeBusinessOpportunityComponent,
    KssBoxOfficeDistributionComponent,
    KssBoxOfficeFilmExhibitionComponent,
    KssBoxOfficeFilmyCarvanComponent,
    KssBoxOfficeManagementTeamComponent,
    KssBoxOfficeMovieDetailsComponent,
    KssBoxOfficeMoviesScheduleComponent,
    KssBoxOfficeProductionComponent,
    KssBoxOfficeWhatWeDoComponent,
    KssBoxOfficeWhoWeAreComponent,
    LoginComponent,
    MembershipRegistrationComponent,
    RegisterComponent

  ],
  providers: [ HttpService ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
