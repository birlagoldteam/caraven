import { Component, OnInit } from '@angular/core';
import { HttpService } from '../services/http.service';
import { NgModel } from '@angular/forms';



@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers: [HttpService],
  exportAs: 'ngModel'
})
export class HomeComponent implements OnInit {

public f_name: string;
public l_name: string;
public getData: string;

response: string;

constructor(private httpService: HttpService) {}

ngOnInit(){

}


clicked(){

 this.httpService.getPosts( this.f_name, this.l_name ).
 subscribe(response => this.response = response,
          error => console.log(error))

}


clicktogetdata(){

 this.httpService.getData().
 subscribe(data =>this.getData = JSON.stringify(data),
           error => alert(error),
           () => console.log('Finished'))
   
}

}
