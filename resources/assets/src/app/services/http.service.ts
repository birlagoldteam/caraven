import { Component } from '@angular/core';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Http, Headers, Response  } from '@angular/http';
import 'rxjs/Rx';

import { FormsModule } from '@angular/forms';
import { NgModel } from '@angular/forms';
import { URLSearchParams } from "@angular/http";
import { EventEmitter, Input, Output} from '@angular/core';



@Injectable()
export class HttpService {


constructor(private http: Http) {}


getPosts(fname: string, lname: string ) {

  let data = new URLSearchParams();
  data.append('firstname', fname);
  data.append('lastname', lname);
  
  return this.http.post('/home', data)
   .map(res => res.json())
}



getData() {
	
     return this.http.get('/home')
    .map(res => res.json())
}


}