<!doctype html>
<html lang="{{ config('app.locale') }}">
    <head>
        <base href="/">
       <!-- Basic -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">   

        <title>K Sera Sera - Box Office | Filmy Caravan</title> 

        <meta name="keywords" content="K Sera Sera, Box Office, Filmy Caravan" />
        <meta name="description" content="K Sera Sera - Box Office | Filmy Caravan">
        <meta name="author" content="Mr. Sukhsagar Kumawat">

        <!-- Favicon -->
        <link rel="shortcut icon" href="js/assets/carvan_assets/img/favicon.ico" type="image/x-icon" />
        <link rel="apple-touch-icon" href="js/assets/carvan_assets/img/apple-touch-icon.png">

        <!-- <link rel="icon" type="image/png" href="/favicon-32x32.png" sizes="16x16"> -->
        <link rel="stylesheet" href="css/bootstrap.min.css"> 

        <!-- Mobile Metas -->
        <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">   
        <!-- <title>Filmy Carvaan</title> -->

        <!-- Web Fonts  -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light" rel="stylesheet" type="text/css">

        <!-- Vendor CSS -->
        <link rel="stylesheet" href="js/assets/carvan_assets/vendor/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="js/assets/carvan_assets/vendor/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="js/assets/carvan_assets/vendor/animate/animate.min.css">
        <link rel="stylesheet" href="js/assets/carvan_assets/vendor/simple-line-icons/css/simple-line-icons.min.css">
        <link rel="stylesheet" href="js/assets/carvan_assets/vendor/owl.carousel/assets/owl.carousel.min.css">
        <link rel="stylesheet" href="js/assets/carvan_assets/vendor/owl.carousel/assets/owl.theme.default.min.css">
        <link rel="stylesheet" href="js/assets/carvan_assets/vendor/magnific-popup/magnific-popup.min.css">


        <!-- Theme CSS -->
        <link rel="stylesheet" href="js/assets/carvan_assets/css/theme.css">
        <link rel="stylesheet" href="js/assets/carvan_assets/css/theme-elements.css">
        <link rel="stylesheet" href="js/assets/carvan_assets/css/theme-blog.css">
        <link rel="stylesheet" href="js/assets/carvan_assets/css/theme-shop.css">

        <!-- Current Page CSS -->
        <link rel="stylesheet" href="js/assets/carvan_assets/vendor/rs-plugin/css/settings.css">
        <link rel="stylesheet" href="js/assets/carvan_assets/vendor/rs-plugin/css/layers.css">
        <link rel="stylesheet" href="js/assets/carvan_assets/vendor/rs-plugin/css/navigation.css">

        <!-- Skin CSS -->
        <link rel="stylesheet" href="js/assets/carvan_assets/css/skins/skin-real-estate.css"> 

        <!-- Demo CSS -->
        <link rel="stylesheet" href="js/assets/carvan_assets/css/demos/demo-real-estate.css">

        <!-- Theme Custom CSS -->
        <link rel="stylesheet" href="js/assets/carvan_assets/css/custom.css">

        <!-- Head Libs -->
        <script src="js/assets/carvan_assets/vendor/modernizr/modernizr.min.js"></script>
        
    </head>

    <body>
        <div class="loading-overlay">
            <div class="bounce-loader">
                <div class="bounce1"></div>
                <div class="bounce2"></div>
                <div class="bounce3"></div>
            </div>
        </div>



        <div class="body">

        <app-root></app-root>
           <!--  <?php 
                // include 'header.php';
            ?> -->
        </div>

        <!--  <app-root></app-root> -->

        <script type="text/javascript" src="js/inline.bundle.js"></script>
        <script type="text/javascript" src="js/polyfills.bundle.js"></script>
        <script type="text/javascript" src="js/styles.bundle.js"></script>
        <script type="text/javascript" src="js/vendor.bundle.js"></script>
        <script type="text/javascript" src="js/main.bundle.js"></script>
        <script type="text/javascript" src="js/assets/jquery-3.3.1.min.js"></script>
        <script type="text/javascript" src="js/assets/bootstrap.min.js"></script>


        <!-- Vendor -->
        <script src="js/assets/carvan_assets/vendor/jquery/jquery.min.js"></script>
        <script src="js/assets/carvan_assets/vendor/jquery.appear/jquery.appear.min.js"></script>
        <script src="js/assets/carvan_assets/vendor/jquery.easing/jquery.easing.min.js"></script>
        <script src="js/assets/carvan_assets/vendor/jquery-cookie/jquery-cookie.min.js"></script>
        <script src="js/assets/carvan_assets/vendor/bootstrap/js/bootstrap.min.js">
        </script>
        <script src="js/assets/carvan_assets/vendor/common/common.min.js"></script>
        <script src="js/assets/carvan_assets/vendor/jquery.validation/jquery.validation.min.js"></script>
        <script src="js/assets/carvan_assets/vendor/jquery.easy-pie-chart/jquery.easy-pie-chart.min.js"></script>
        <script src="js/assets/carvan_assets/vendor/jquery.gmap/jquery.gmap.min.js">
        </script>
        <script src="js/assets/carvan_assets/vendor/jquery.lazyload/jquery.lazyload.min.js"></script>
        <script src="js/assets/carvan_assets/vendor/isotope/jquery.isotope.min.js">
        </script>
        <script src="js/assets/carvan_assets/vendor/owl.carousel/owl.carousel.min.js"></script>
        <script src="js/assets/carvan_assets/vendor/magnific-popup/jquery.magnific-popup.min.js"></script>
        <script src="js/assets/carvan_assets/vendor/vide/vide.min.js"></script>
        
        <!-- Theme Base, Components and Settings -->
        <script src="js/assets/carvan_assets/js/theme.js"></script>
        
        <!-- Current Page Vendor and Views -->
        <script src="js/assets/carvan_assets/vendor/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
        <script src="js/assets/carvan_assets/vendor/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>

        <!-- Current Page Vendor and Views -->
        <script src="js/assets/carvan_assets/js/views/view.contact.js"></script>

        <!-- Demo -->
        <script src="js/assets/carvan_assets/js/demos/demo-real-estate.js"></script>
        
        <!-- Theme Custom -->
        <script src="js/assets/carvan_assets/js/custom.js"></script>
        
        <!-- Theme Initialization Files -->
        <script src="js/assets/carvan_assets/js/theme.init.js"></script>
    
    </body>
</html>